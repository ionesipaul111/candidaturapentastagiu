<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
    <%@ page import="AgentieFilmeModel.*, java.util.*, AgentieFilmeInterogation.*" %>
    
    <%List<Filme> result= new ArrayList<Filme>();
    FilmeDAO filmeDao= new FilmeDAO(ConnectionFactory.getConnection());
    result=filmeDao.loadAll(); %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />

<title>Agentie filme </title>
</head>
<body>

	<div id="complet">
	
	
	<div id="header">
		
	
		
		
	</div>
	
	<div id="nav">
	
	<ul class="blue">

	<li><a href="index.jsp">Home</a></li>
	<li><a href="actori.jsp">Actori</a></li>
	<li><a href="filme.jsp">Filme</a></li>
    <li><a href="genuri.jsp">Genuri</a></li>
    <li><a href="distributie.jsp">Distributie</a></li>
     <li><a href="login.jsp">Login</a></li>
  
</ul>
	
	
	</div>
	
	<div id="continut">
	
	<table style="width: 99%;" border="1" cellpadding="0" cellspacing="2">
		<tr>
			<td>ID film</td>
			<td>Id gen</td>
			<td>Titlu film eng</td>
			<td>Titlu film ro</td>
			<td>An lansare</td>

		</tr>

	<% for(Filme object:result) { %>
			<tr>
				<td><%=object.getId_film() %></td>
				<td><%=object.getId_gen() %></td>
				<td><%=object.getTitlu_film_eng() %></td>
				<td><%=object.getTitlu_film_ro() %></td>
				<td><%=object.getAn_lansare() %></td>
				
			</tr>
			<%} %>
	</table>
	
	</div>
	
	
	
	
	
	<div id="footer">
		Copyright 2015 � Ionesi Paul
	</div>
	
</div>
	
	









</body>
</html>
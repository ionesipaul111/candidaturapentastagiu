<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="style.css" />

<title>Agentie filme </title>
</head>
<body>
	<div id="complet">
	
	<div id="header">
				
	</div>
		
	<div id="nav">
	<ul class="blue">

	<li><a href="index.jsp">Home</a></li>
	<li><a href="actori.jsp">Actori</a></li>
	<li><a href="filme.jsp">Filme</a></li>
    <li><a href="genuri.jsp">Genuri</a></li>
    <li><a href="distributie.jsp">Distributie</a></li>
     <li><a href="login.jsp">Login</a></li>
 
	</ul>
	</div>
	
	<div id="continut">
	
		<div id="actori">
	
	<form method="post" action="agentie.do">
	<table align="left">

	<tr>
	<td>Insert Nume actor</td>
	<td><input type="text" name="nume_actor" value=""></td>
			
	<tr>
	<td>Insert data nasteri</td>
	<td><input type="text" name="data_nasteri" value=""></td></tr>
	
	<tr>
	<td>Insert nationalitate</td>
	<td><input type="text" name="nationalitate" value=""></td></tr>
	
	</table>
	<br><br><br><br>
	<input type="hidden" name="action" value="insertActor"/>

	<input type="submit" value="Insert Actor"/>
	<input type="reset" value="Reset"/> 
	</form>
	<br><br>
		
	<form method="post" action="agentie.do">
	<table align="left">
	
	<tr>
	<td>Insert ID actor</td>
	<td><input type="text" name="id_actor" value=""></td>
	
	<tr>
	<td>Insert Nume actor</td>
	<td><input type="text" name="nume_actor" value=""></td>
			
	<tr>
	<td>Insert data nasteri</td>
	<td><input type="text" name="data_nasteri" value=""></td></tr>
	
	<tr>
	<td>Insert nationalitate</td>
	<td><input type="text" name="nationalitate" value=""></td></tr>
	
	
	</table>
	<br><br><br><br><br>

	<input type="hidden" name="action" value="updateActor"/>

	<input type="submit" value="Update Actor"/>
	<input type="reset" value="Reset"/> 
	</form>	
	<br><br>
	
	<form method="get" action="agentie.do">

Insert id :<input type="text" name="id" value=""/>
<br><br>

<input type="hidden" name="action" value="deleteActor"/>

<input type="submit" value="Delete Actor"/>
	</form>
	
	
	</div>
	
	<div id="filme">
	
	<form method="post" action="agentie.do">
	<table align="left">

	<tr>
	<td>Insert ID gen</td>
	<td><input type="text" name="id_gen" value=""></td>
			
	<tr>
	<td>Insert titlu film eng</td>
	<td><input type="text" name="titlu_film_eng" value=""></td></tr>
	
	<tr>
	<td>Insert titlu film eng</td>
	<td><input type="text" name="titlu_film_ro" value=""></td></tr>
		<tr>
	<td>Insert an lansare</td>
	<td><input type="text" name="an_lansare" value=""></td></tr>
	
	</table>
	<br><br><br><br>
	<input type="hidden" name="action" value="insertFilm"/>

	<input type="submit" value="Insert Film"/>
	<input type="reset" value="Reset"/> 
	</form>
	<br><br>
		
	<form method="post" action="agentie.do">
	<table align="left">
	
	<tr>
	<td>Insert ID film</td>
	<td><input type="text" name="id_film" value=""></td>
	
	<tr>
	<td>Insert ID gen</td>
	<td><input type="text" name="id_gen" value=""></td>
	
	<tr>
	<td>Insert titlu film eng</td>
	<td><input type="text" name="titlu_film_eng" value=""></td>
			
	<tr>
	<td>Insert titlu_film_ro</td>
	<td><input type="text" name="titlu_film_ro" value=""></td></tr>
	
	<tr>
	<td>Insert an lansare</td>
	<td><input type="text" name="an_lansare" value=""></td></tr>
	
	
	</table>
	<br><br><br><br><br>

	<input type="hidden" name="action" value="updateFilm"/>

	<input type="submit" value="Update Film"/>
	<input type="reset" value="Reset"/> 
	</form>	
	<br><br>
	
	<form method="get" action="agentie.do">

Insert id :<input type="text" name="id" value=""/>
<br><br>

<input type="hidden" name="action" value="deleteFilm"/>

<input type="submit" value="Delete Film"/>
	</form>
	
	</div>
	
	<div id="genuri">
	
	<form method="post" action="agentie.do">
	<table align="left">

	<tr>
	<td>Insert nume gen</td>
	<td><input type="text" name="nume_gen" value=""></td>
			
	</table>
	<br><br><br><br>
	<input type="hidden" name="action" value="insertGen"/>

	<input type="submit" value="Insert Gen"/>
	<input type="reset" value="Reset"/> 
	</form>
	<br><br>
		
	<form method="post" action="agentie.do">
	<table align="left">
	
	<tr>
	<td>Insert ID gen</td>
	<td><input type="text" name="id_gen" value=""></td>
	
	<tr>
	<td>Insert nume gen</td>
	<td><input type="text" name="nume_gen" value=""></td>
		
	</table>
	<br><br><br><br><br>

	<input type="hidden" name="action" value="updateGen"/>

	<input type="submit" value="Update Gen"/>
	<input type="reset" value="Reset"/> 
	</form>	
	<br><br>
	
	<form method="get" action="agentie.do">

	Insert id :<input type="text" name="id" value=""/>
	<br><br>

	<input type="hidden" name="action" value="deleteGen"/>

	<input type="submit" value="Delete Gen"/>
	</form>		
	</div>
	
	<div id="distributie">
	<form method="post" action="agentie.do">
	<table align="left">

	<tr>
	<td>Insert ID Film</td>
	<td><input type="text" name="id_film" value=""></td>
	
	<tr>
	<td>Insert ID gen</td>
	<td><input type="text" name="id_gen" value=""></td>
	
	<tr>
	<td>Insert ID actor</td>
	<td><input type="text" name="id_actor" value=""></td>
	
	<tr>
	<td>Insert rol</td>
	<td><input type="text" name="rol" value=""></td></tr>
	
	</table>
	<br><br><br><br>
	<input type="hidden" name="action" value="insertDistributie"/>

	<input type="submit" value="Insert Distributie"/>
	<input type="reset" value="Reset"/> 
	</form>
	<br><br>
		
	<form method="post" action="agentie.do">
	<table align="left">
	
	<tr>
	<td>Insert ID film</td>
	<td><input type="text" name="id_film" value=""></td>
	
	<tr>
	<td>Insert ID gen</td>
	<td><input type="text" name="id_gen" value=""></td>
	
	<tr>
	<td>Insert ID actor</td>
	<td><input type="text" name="id_actor" value=""></td>
			
	<tr>
	<td>Insert rol</td>
	<td><input type="text" name="rol" value=""></td></tr>
	
	
	</table>
	<br><br><br><br><br>

	<input type="hidden" name="action" value="updateDistributie"/>

	<input type="submit" value="Update Distributie"/>
	<input type="reset" value="Reset"/> 
	</form>	
	<br><br>
	
	<form method="get" action="agentie.do">

	Insert id :<input type="text" name="id" value=""/>
	<br><br>

	<input type="hidden" name="action" value="deleteDistributie"/>

	<input type="submit" value="Delete Distributie"/>
	</form>
	
	
	</div>
	
	
	
	
	
	
	
	
	
	
	
	
	
	</div>
	
	
	
	
	
	<div id="footer">
		Copyright 2015 � Ionesi Paul
	</div>
	
</div>
	
	









</body>
</html>
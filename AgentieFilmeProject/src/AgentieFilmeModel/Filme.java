package AgentieFilmeModel;

import java.util.Date;

public class Filme {
	
	private long id_film;
	private long id_gen;
	private String titlu_film_eng;
	private String titlu_film_ro;
	private Date an_lansare;
	
	private String nume_gen;
	
	public String toString() {
		return "Filme [id_film=" + id_film + ", id_gen=" + id_gen + ",titlu_film_eng="
				+titlu_film_eng + ", titlu_film_ro=" + titlu_film_ro + ", an_lansare=" + an_lansare + "]";
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((an_lansare == null) ? 0 : an_lansare.hashCode());
		result = prime * result + (int) (id_film ^ (id_film >>> 32));
		result = prime * result + (int) (id_gen ^ (id_gen >>> 32));
		result = prime * result
				+ ((nume_gen == null) ? 0 : nume_gen.hashCode());
		result = prime * result
				+ ((titlu_film_eng == null) ? 0 : titlu_film_eng.hashCode());
		result = prime * result
				+ ((titlu_film_ro == null) ? 0 : titlu_film_ro.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Filme other = (Filme) obj;
		if (an_lansare == null) {
			if (other.an_lansare != null)
				return false;
		} else if (!an_lansare.equals(other.an_lansare))
			return false;
		if (id_film != other.id_film)
			return false;
		if (id_gen != other.id_gen)
			return false;
		if (nume_gen == null) {
			if (other.nume_gen != null)
				return false;
		} else if (!nume_gen.equals(other.nume_gen))
			return false;
		if (titlu_film_eng == null) {
			if (other.titlu_film_eng != null)
				return false;
		} else if (!titlu_film_eng.equals(other.titlu_film_eng))
			return false;
		if (titlu_film_ro == null) {
			if (other.titlu_film_ro != null)
				return false;
		} else if (!titlu_film_ro.equals(other.titlu_film_ro))
			return false;
		return true;
	}
	public long getId_film() {
		return id_film;
	}
	public void setId_film(long id_film) {
		this.id_film = id_film;
	}
	public long getId_gen() {
		return id_gen;
	}
	public void setId_gen(long id_gen) {
		this.id_gen = id_gen;
	}
	public String getTitlu_film_eng() {
		return titlu_film_eng;
	}
	public void setTitlu_film_eng(String titlu_film_eng) {
		this.titlu_film_eng = titlu_film_eng;
	}
	public String getTitlu_film_ro() {
		return titlu_film_ro;
	}
	public void setTitlu_film_ro(String titlu_film_ro) {
		this.titlu_film_ro = titlu_film_ro;
	}
	public Date getAn_lansare() {
		return an_lansare;
	}
	public void setAn_lansare(Date an_lansare) {
		this.an_lansare = an_lansare;
	}


	public String getNume_gen() {
		return nume_gen;
	}


	public void setNume_gen(String nume_gen) {
		this.nume_gen = nume_gen;
	}
	
	
	
	

}

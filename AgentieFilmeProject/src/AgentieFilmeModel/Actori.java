package AgentieFilmeModel;

import java.util.Date;


public class Actori {
	
	private long id_actor;
	private String nume_actor;
	private Date data_nasteri;
	private String nationalitate;
	
	public String toString() {
		return "Actori [id_actor=" + id_actor + ", nume_actor=" + nume_actor + ",data_nasteri="
				+data_nasteri + ", nationalitate=" + nationalitate + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((data_nasteri == null) ? 0 : data_nasteri.hashCode());
		result = prime * result + (int) (id_actor ^ (id_actor >>> 32));
		result = prime * result
				+ ((nationalitate == null) ? 0 : nationalitate.hashCode());
		result = prime * result
				+ ((nume_actor == null) ? 0 : nume_actor.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Actori other = (Actori) obj;
		if (data_nasteri == null) {
			if (other.data_nasteri != null)
				return false;
		} else if (!data_nasteri.equals(other.data_nasteri))
			return false;
		if (id_actor != other.id_actor)
			return false;
		if (nationalitate == null) {
			if (other.nationalitate != null)
				return false;
		} else if (!nationalitate.equals(other.nationalitate))
			return false;
		if (nume_actor == null) {
			if (other.nume_actor != null)
				return false;
		} else if (!nume_actor.equals(other.nume_actor))
			return false;
		return true;
	}
	public long getId_actor() {
		return id_actor;
	}
	public void setId_actor(long id_actor) {
		this.id_actor = id_actor;
	}
	public String getNume_actor() {
		return nume_actor;
	}
	public void setNume_actor(String nume_actor) {
		this.nume_actor = nume_actor;
	}
	public Date getData_nasteri() {
		return data_nasteri;
	}
	public void setData_nasteri(Date data_nasteri) {
		this.data_nasteri = data_nasteri;
	}
	public String getNationalitate() {
		return nationalitate;
	}
	public void setNationalitate(String nationalitate) {
		this.nationalitate = nationalitate;
	}
	

}

package AgentieFilmeModel;

public class Genuri {
	
	private long id_gen;
	private String nume_gen;
	
	public String toString() {
		return "Genuri [id_gen=" + id_gen + ", nume_gen=" + nume_gen + "]";
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id_gen ^ (id_gen >>> 32));
		result = prime * result
				+ ((nume_gen == null) ? 0 : nume_gen.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Genuri other = (Genuri) obj;
		if (id_gen != other.id_gen)
			return false;
		if (nume_gen == null) {
			if (other.nume_gen != null)
				return false;
		} else if (!nume_gen.equals(other.nume_gen))
			return false;
		return true;
	}
	public long getId_gen() {
		return id_gen;
	}
	public void setId_gen(long id_gen) {
		this.id_gen = id_gen;
	}
	public String getNume_gen() {
		return nume_gen;
	}
	public void setNume_gen(String nume_gen) {
		this.nume_gen = nume_gen;
	}
	
	
	
}

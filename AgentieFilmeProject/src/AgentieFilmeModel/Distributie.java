package AgentieFilmeModel;

public class Distributie {
	
	private long id_film;
	private long id_gen;
	private long id_actor;
	private String rol;
	
	private String nume_gen;
	
	public String toString() {
		return "Filme [id_film=" + id_film + ", id_gen=" + id_gen + ",id_actor="	+id_actor  + ",rol="	+rol  + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id_actor ^ (id_actor >>> 32));
		result = prime * result + (int) (id_film ^ (id_film >>> 32));
		result = prime * result + (int) (id_gen ^ (id_gen >>> 32));
		result = prime * result
				+ ((nume_gen == null) ? 0 : nume_gen.hashCode());
		result = prime * result + ((rol == null) ? 0 : rol.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Distributie other = (Distributie) obj;
		if (id_actor != other.id_actor)
			return false;
		if (id_film != other.id_film)
			return false;
		if (id_gen != other.id_gen)
			return false;
		if (nume_gen == null) {
			if (other.nume_gen != null)
				return false;
		} else if (!nume_gen.equals(other.nume_gen))
			return false;
		if (rol == null) {
			if (other.rol != null)
				return false;
		} else if (!rol.equals(other.rol))
			return false;
		return true;
	}

	public long getId_film() {
		return id_film;
	}

	public void setId_film(long id_film) {
		this.id_film = id_film;
	}

	public long getId_gen() {
		return id_gen;
	}

	public void setId_gen(long id_gen) {
		this.id_gen = id_gen;
	}

	public long getId_actor() {
		return id_actor;
	}

	public void setId_actor(long id_actor) {
		this.id_actor = id_actor;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public String getNume_gen() {
		return nume_gen;
	}

	public void setNume_gen(String nume_gen) {
		this.nume_gen = nume_gen;
	}
	
	

	
	
	
	
}

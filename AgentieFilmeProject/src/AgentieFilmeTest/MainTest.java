package AgentieFilmeTest;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import AgentieFilmeInterogation.ActoriDAO;
import AgentieFilmeInterogation.ConnectionFactory;
import AgentieFilmeInterogation.DistributieDAO;
import AgentieFilmeInterogation.FilmeDAO;
import AgentieFilmeInterogation.GenuriDAO;
import AgentieFilmeModel.Actori;
import AgentieFilmeModel.Distributie;
import AgentieFilmeModel.Filme;
import AgentieFilmeModel.Genuri;

public class MainTest {

	static ActoriDAO actor;
	static FilmeDAO film;
	static GenuriDAO gen;
	static DistributieDAO distributie;
	
	public static void main(String[] args) throws  InstantiationException,
	IllegalAccessException, ClassNotFoundException, SQLException{
		
		actor=new ActoriDAO(ConnectionFactory.getConnection());
		film=new FilmeDAO(ConnectionFactory.getConnection());
		gen=new GenuriDAO(ConnectionFactory.getConnection());
		distributie=new DistributieDAO(ConnectionFactory.getConnection());
		
		
		//insertStatementTest();
		//loadStatementByIdTest();
		//updateStatementTest();
		//deleteStatementTest();
		//loadAllStatementTest();
		//saveStatementTest();
		//searchStatementTest();
		//loadAllStatementTestFilme();
		//loadAllStatementTestGenuri();
		//loadAllStatementTestDistributie();
		searchActor();
	
	}
	
	private static void searchActor() throws SQLException{
		List<Actori> res=actor.search((long) 1000);
		displayAllList(res);
	}

	private static void loadAllStatementTest() throws SQLException{
		List<Actori> res=actor.loadAll();
		displayAllList(res);
	}
	
	private static void loadAllStatementTestFilme() throws SQLException{
		List<Filme> res=film.loadAll();
		displayAllListFilme(res);
	}
	
	
	private static void loadAllStatementTestGenuri() throws SQLException{
		List<Genuri> res=gen.loadAll();
		displayAllListGenuri(res);
	}
	
	private static void loadAllStatementTestDistributie() throws SQLException{
		List<Distributie> res=distributie.loadAll();
		displayAllListDistributie(res);
	}
	
	public static void displayAllList(List<Actori> res){
		System.out.println("-------------------------------");
		for(Actori obj:res){
			System.out.println(obj);
		}
		System.out.println("-------------------------------");
	}
	
	public static void displayAllListFilme(List<Filme> res){
		System.out.println("-------------------------------");
		for(Filme obj:res){
			System.out.println(obj);
		}
		System.out.println("-------------------------------");
	}
	
	public static void displayAllListGenuri(List<Genuri> res){
		System.out.println("-------------------------------");
		for(Genuri obj:res){
			System.out.println(obj);
		}
		System.out.println("-------------------------------");
	}
	
	public static void displayAllListDistributie(List<Distributie> res){
		System.out.println("-------------------------------");
		for(Distributie obj:res){
			System.out.println(obj);
		}
		System.out.println("-------------------------------");
	}
		
	private static void insertStatementTest() {
		Actori actorObject = new Actori();
		actorObject.setId_actor(2000);
		actorObject.setNume_actor("Actor");
		actorObject.setData_nasteri(new Date());
		actorObject.setNationalitate("american");

		actor.insert(actorObject);
	}
	
	private static void updateStatementTest() {
		Actori actorObject = new Actori();
		actorObject.setId_actor(2001);
		actorObject.setNume_actor("Author1");
		actorObject.setData_nasteri(new Date());
		actorObject.setNationalitate("american");

		actor.update(actorObject);
	}
	

	public static void deleteStatementTest() {

			List<Actori> result = actor.loadAll();
			if(!result.isEmpty()){
			actor.delete(result.get(0).getId_actor());
			}
		
	}
	
	
	
	
	
	
}

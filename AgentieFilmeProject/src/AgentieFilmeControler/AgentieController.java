package AgentieFilmeControler;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import AgentieFilmeInterogation.ActoriDAO;
import AgentieFilmeInterogation.ConnectionFactory;
import AgentieFilmeInterogation.DistributieDAO;
import AgentieFilmeInterogation.FilmeDAO;
import AgentieFilmeInterogation.GenuriDAO;
import AgentieFilmeInterogation.UserDAO;
import AgentieFilmeModel.Actori;
import AgentieFilmeModel.Distributie;
import AgentieFilmeModel.Filme;
import AgentieFilmeModel.Genuri;


public class AgentieController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			String action= (String) request.getParameter("action");
			/*	
	
	if(action.equals("getAllActors"));
		{
			getAllActors(request, response);
		}
	*/
	if(action.equals("deleteActor"));
		{
			deleteActor(request, response);
		}
	if(action.equals("deleteFilm"));
		{
			deleteFilm(request, response);
		}
	if(action.equals("deleteGen"));
		{
			deleteGen(request, response);
		}
	if(action.equals("deleteDistributie"));
		{
			deleteDistr(request, response);
		}
	if(action.equals("genSearch")){
			searchGen(request, response);
		}
	if(action.equals("searchActor")){
			searchActor(request,response);
	}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = (String) request.getParameter("action");
		
		if(action.equals("login")){
			loginUser(request, response);
		}
		if(action.equals("insertActor")){
			insertActor(request,response);
		}	
		if(action.equals("updateActor")){
			updateActor(request,response);
		}	
		if(action.equals("insertFilm")){
			insertFilm(request, response);
		}
		if(action.equals("updateFilm")){
			updateFilm(request,response);
			}
		if(action.equals("insertGen")){
			insertGen(request, response);
		}
		if(action.equals("updateGen")){
			updateGen(request,response);
			}
		if(action.equals("insertDistributie")){
			insertDistr(request, response);
		}
		if(action.equals("updateDistributie")){
			updateDistr(request, response);
		}
	}

	private void insertActor(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		
			Actori result=new Actori();
			String nume_actor=(String) request.getParameter("nume_actor");
			String nationalitate=(String) request.getParameter("nationalitate");

			result.setNume_actor(nume_actor);
			result.setNationalitate(nationalitate);
			result.setData_nasteri(new java.sql.Timestamp(new java.util.Date().getTime()));
		
	try{		
			ActoriDAO actorDao = new ActoriDAO(ConnectionFactory.getConnection());
			actorDao.insert(result);			
		}catch(Exception e){
			e.printStackTrace();
		}
	response.sendRedirect("iud.jsp");
		
	}

	private void updateActor(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		Actori result=new Actori();	
		
		String id=(String) request.getParameter("id_actor");
				
		String nume_actor=(String) request.getParameter("nume_actor");
		String nationalitate=(String) request.getParameter("nationalitate");

			
	try{		
		ActoriDAO actoriDao = new ActoriDAO(ConnectionFactory.getConnection());
			result=actoriDao.load(Long.parseLong(id));
			result.setId_actor(Long.parseLong(id));
			if(nume_actor!=""){
			result.setNume_actor(nume_actor);}
			result.setData_nasteri(new java.sql.Timestamp(new java.util.Date().getTime()));
			if(nationalitate!=""){
			result.setNationalitate(nationalitate);	}
			actoriDao.update(result);	
						
		}catch(Exception e){
			e.printStackTrace();
		}
	response.sendRedirect("iud.jsp");
	}

	private void deleteActor(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		
		String id=(String) request.getParameter("id");
	
				
	try{		
			ActoriDAO actorDao = new ActoriDAO(ConnectionFactory.getConnection());
			actorDao.delete(Long.parseLong(id));			
			
		}catch(Exception e){
			e.printStackTrace();
		}
	
	}
		
	private void insertFilm(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		
			Filme result=new Filme();
			
			String id_gen=(String) request.getParameter("id_gen");
			String titlu_film_eng=(String) request.getParameter("titlu_film_eng");
			String titlu_film_ro=(String) request.getParameter("titlu_film_ro");

			result.setId_gen(Long.parseLong(id_gen));
			result.setTitlu_film_eng(titlu_film_eng);
			result.setTitlu_film_ro(titlu_film_ro);
			result.setAn_lansare(new java.sql.Timestamp(new java.util.Date().getTime()));
		
	try{		
		FilmeDAO filmDao = new FilmeDAO(ConnectionFactory.getConnection());
			filmDao.insert(result);			
		}catch(Exception e){
			e.printStackTrace();
		}
	response.sendRedirect("iud.jsp");
	}
	
	private void updateFilm(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		Filme result=new Filme();	
		
		String id=(String) request.getParameter("id_film");
				
		String id_gen=(String) request.getParameter("id_gen");
		String titlu_film_eng=(String) request.getParameter("titlu_film_eng");
		String titlu_film_ro=(String) request.getParameter("titlu_film_ro");

			
	try{		
		FilmeDAO filmeDao = new FilmeDAO(ConnectionFactory.getConnection());
		result=filmeDao.load(Long.parseLong(id));
		result.setId_film(Long.parseLong(id));
		if(id_gen!=""){
		result.setId_gen(Long.parseLong(id_gen));}
		if(titlu_film_eng!=""){
		result.setTitlu_film_eng(titlu_film_eng);}
		if(titlu_film_ro!=""){
		result.setTitlu_film_ro(titlu_film_ro);}
		result.setAn_lansare(new java.sql.Timestamp(new java.util.Date().getTime()));
		
		filmeDao.update(result);					
		
		}catch(Exception e){
			e.printStackTrace();
		}
	response.sendRedirect("iud.jsp");
	}

	private void deleteFilm(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		
		String id=(String) request.getParameter("id");
	
				
	try{		
			FilmeDAO filmDao = new FilmeDAO(ConnectionFactory.getConnection());
			filmDao.delete(Long.parseLong(id));			
			
		}catch(Exception e){
			e.printStackTrace();
		}
	
	}
		
	private void insertGen(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
			
			Genuri result=new Genuri();
			
			String nume_gen=(String) request.getParameter("nume_gen");

			result.setNume_gen(nume_gen);	
	try{		
		GenuriDAO genDao = new GenuriDAO(ConnectionFactory.getConnection());
			genDao.insert(result);			
		}catch(Exception e){
			e.printStackTrace();
		}
	response.sendRedirect("iud.jsp");
	}
	
	private void updateGen(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		Genuri result=new Genuri();	
		
		String id=(String) request.getParameter("id_gen");
				
		String nume_gen=(String) request.getParameter("nume_gen");


			
	try{		
		GenuriDAO genuriDao = new GenuriDAO(ConnectionFactory.getConnection());
		result=genuriDao.load(Long.parseLong(id));
		result.setId_gen(Long.parseLong(id));
		if(nume_gen!=""){
		result.setNume_gen(nume_gen);
		}	
		genuriDao.update(result);
		
		}catch(Exception e){
			e.printStackTrace();
		}
	response.sendRedirect("iud.jsp");
	}
		
	private void deleteGen(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		
		String id=(String) request.getParameter("id");
	
				
	try{		
			GenuriDAO genDao = new GenuriDAO(ConnectionFactory.getConnection());
			genDao.delete(Long.parseLong(id));			
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
		
	private void insertDistr(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		
			Distributie result=new Distributie();
			
			String id_film=(String) request.getParameter("id_film");
			String id_gen=(String) request.getParameter("id_gen");
			String id_actor=(String) request.getParameter("id_actor");
			String rol=(String) request.getParameter("rol");
			

			result.setId_film(Long.parseLong(id_film));
			result.setId_gen(Long.parseLong(id_gen));
			result.setId_actor(Long.parseLong(id_actor));
			result.setRol(rol);
			
		
	try{		
		DistributieDAO distributieDao = new DistributieDAO(ConnectionFactory.getConnection());
			distributieDao.insert(result);			
		}catch(Exception e){
			e.printStackTrace();
		}
	response.sendRedirect("iud.jsp");
	}
	
	private void updateDistr(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		Distributie result=new Distributie();	
		
		String id=(String) request.getParameter("id_film");
				
		String id_gen=(String) request.getParameter("id_gen");
		String id_actor=(String) request.getParameter("id_actor");
		String rol=(String) request.getParameter("rol");

			
	try{		
		DistributieDAO distributieDao = new DistributieDAO(ConnectionFactory.getConnection());
		result=distributieDao.load(Long.parseLong(id));
		result.setId_film(Long.parseLong(id));
		if(id_gen!=""){
		result.setId_gen(Long.parseLong(id_gen));}
		if(id_actor!=""){
		result.setId_actor(Long.parseLong(id_actor));}
		if(rol!=""){
		result.setRol(rol);}
		
		
		distributieDao.update(result);					
		
		}catch(Exception e){
			e.printStackTrace();
		}
	response.sendRedirect("iud.jsp");
	}

	private void deleteDistr(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String id=(String) request.getParameter("id");
				
	try{		
			DistributieDAO distributieDao = new DistributieDAO(ConnectionFactory.getConnection());
			distributieDao.delete(Long.parseLong(id));			
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private void searchGen(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	
		String nume_gen=(String) request.getParameter("nume_gen");
		Genuri gen=new Genuri();
		List<Filme> filme=new ArrayList<Filme>();
		try {
			GenuriDAO genDao= new GenuriDAO(ConnectionFactory.getConnection());
			gen=genDao.returnCod(nume_gen);		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			long id=gen.getId_gen();
			FilmeDAO filmeDao = new FilmeDAO(ConnectionFactory.getConnection());
			filme=filmeDao.genSearch(id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		request.setAttribute("listOfFilme", filme);
		RequestDispatcher view = request.getRequestDispatcher("searchGen.jsp");
		view.forward(request, response);
	
	}
	
	private void searchActor(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
	
		String id_actor=(String) request.getParameter("id_actor");
		List<Distributie> actori=new ArrayList<Distributie>();
		try {
			DistributieDAO actoriDao= new DistributieDAO(ConnectionFactory.getConnection());
			actori=actoriDao.search(Long.parseLong(id_actor));		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		request.setAttribute("listOfActori", actori);
		RequestDispatcher view = request.getRequestDispatcher("searchActori.jsp");
		view.forward(request, response);
	
	}
		
	private void loginUser(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		
		String user = (String) request.getParameter("username");
		String pass = (String) request.getParameter("password");
		
		try {
			UserDAO userDao = new UserDAO(ConnectionFactory.getConnection());
			if(userDao.validate(user, pass)){
				response.sendRedirect("iud.jsp");
				request.getSession().setAttribute("username", user);
			}else{
				PrintWriter out = response.getWriter();
				out.print("<h1 colo=red> Must to login to edit tables!</h1>");
			}
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

	
	
	/*	
	private void getAllActors(HttpServletRequest request, 
			HttpServletResponse response)throws ServletException, IOException {
		
		List<Actori> result= new ArrayList<Actori>();
		try{
			ActoriDAO actoriDao= new ActoriDAO(ConnectionFactory.getConnection());
			result=actoriDao.loadAll();		
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		request.setAttribute("listOfActors",result);
		RequestDispatcher view = request.getRequestDispatcher("actori.jsp");
		view.forward(request, response);
		return;
		
	}
	
	private void getAllMovies(HttpServletRequest request, 
			HttpServletResponse response)throws ServletException, IOException {
		
		List<Filme> result= new ArrayList<Filme>();
		try{
			FilmeDAO filmeDao= new FilmeDAO(ConnectionFactory.getConnection());
			result=filmeDao.loadAll();		
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		request.setAttribute("listOfMovies",result);
		RequestDispatcher view = request.getRequestDispatcher("filme.jsp");
		view.forward(request, response);
		return;
		
	}
	
	private void getAllGenuri(HttpServletRequest request, 
			HttpServletResponse response)throws ServletException, IOException {
		
		List<Genuri> result= new ArrayList<Genuri>();
		try{
			GenuriDAO genuriDao= new GenuriDAO(ConnectionFactory.getConnection());
			result=genuriDao.loadAll();		
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		request.setAttribute("listOfGenuri",result);
		RequestDispatcher view = request.getRequestDispatcher("genuri.jsp");
		view.forward(request, response);
		return;
		
	}

*/
}

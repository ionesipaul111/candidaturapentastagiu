package AgentieFilmeInterogation;

import java.sql.*;
import java.util.*;

import AgentieFilmeModel.Actori;
import AgentieFilmeModel.Filme;

public class FilmeDAO {
	
	private final Connection connection;
	private final PreparedStatement insertStatement;
	private final PreparedStatement updateStatement;
	private final PreparedStatement deleteStatement;
	private final PreparedStatement loadStatement;
	private final PreparedStatement genStatement;
	
	public FilmeDAO(Connection connection) throws SQLException {
		this.connection = connection;
		loadStatement = connection
				.prepareStatement("SELECT * FROM `agentiefilme`.`filme` WHERE id_film = ?");
		insertStatement = connection.
				prepareStatement("INSERT INTO `agentiefilme`.`filme`(`id_film`,`id_gen`,`titlu_film_eng`,`titlu_film_ro`,`an_lansare`) VALUES ( ?, ?, ?, ?, ?);");
		updateStatement = connection
				.prepareStatement("UPDATE `agentiefilme`.`filme` SET `id_gen` = ?, `titlu_film_eng` = ?, `titlu_film_ro` = ?, `an_lansare` = ? WHERE `id_film` = ? ");
		deleteStatement = connection
				.prepareStatement("DELETE FROM `agentiefilme`.`filme` WHERE id_film = ?");
		genStatement=connection
				.prepareStatement("SELECT `titlu_film_eng`,`titlu_film_ro`, `nume_gen` FROM `agentiefilme`.`filme` f " +
						"JOIN `genuri` `g` ON `f`.`id_gen`=`g`.`id_gen` WHERE `f`.`id_gen` = ?");
		
		
		

	}
	
	public List<Filme> genSearch(long id_gen){
		ResultSet result=null;
		List<Filme> listFilme=new ArrayList<Filme>();
		
		try {
			genStatement.setLong(1,id_gen);
			result = genStatement.executeQuery();
			while(result.next()){
				Filme film=new Filme();
				film.setTitlu_film_eng(result.getString(1));
				film.setTitlu_film_ro(result.getString(2));
				film.setNume_gen(result.getString(3));
				listFilme.add(film);			
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeAll(genStatement,result);
		}
		return listFilme;
		
	}
	
	public Filme load(Long id_film) {
		ResultSet result=null;
	try {
		loadStatement.setLong(1, id_film);
		result = loadStatement.executeQuery();
		if(result.next())
		{
			Filme film=new Filme();
			film.setId_gen(result.getLong(2));
			film.setTitlu_film_eng(result.getString(3));
			film.setTitlu_film_eng(result.getString(4));
			film.setAn_lansare(result.getDate(5));
			return film;
		}
		else {
			return null;
		}			
	} catch (SQLException e) {
		throw new RuntimeException("Error while retrieving film with ID: "
				+ id_film, e);
	}finally{
		ConnectionFactory.closeAll(loadStatement,result);
	}
}
	
	public List<Filme> loadAll() {
		List<Filme> filmResult = new ArrayList<Filme>();
		Statement createStatement=null;
		ResultSet result=null;

		try {
			createStatement = connection.createStatement();
			result = createStatement
					.executeQuery("SELECT * FROM `agentiefilme`.`filme`");

			while(result.next()){
				Filme film = new Filme();
				film.setId_film(result.getLong(1));
				film.setId_gen(result.getLong(2));
				film.setTitlu_film_eng(result.getString(3));
				film.setTitlu_film_ro(result.getString(4));
				film.setAn_lansare(result.getDate(5));
				
				filmResult.add(film);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeAll(createStatement,result);
		}

		return filmResult;
	}
	
	public void insert(Filme film){
		try{
			insertStatement.setLong(1,film.getId_film());
			insertStatement.setLong(2,film.getId_gen());
			insertStatement.setString(3, film.getTitlu_film_eng());			
			insertStatement.setString(4, film.getTitlu_film_ro());
			insertStatement.setDate(5, new java.sql.Date(film.getAn_lansare()
					.getTime()));
			
			insertStatement.execute();
			
		}catch (SQLException e){
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeAll(insertStatement,null);
		}
		
	}
	
	public void update(Filme film){
		try{
		updateStatement.setLong(5, film.getId_film());
		updateStatement.setLong(1, film.getId_gen());
		updateStatement.setString(2, film.getTitlu_film_eng());

		updateStatement.setString(3, film.getTitlu_film_ro());
		updateStatement.setDate(4, new java.sql.Date(film.getAn_lansare()
				.getTime()));
		
		updateStatement.execute();
	
	}catch (SQLException e) {
		e.printStackTrace();
		}finally{
			ConnectionFactory.closeAll(updateStatement,null);
		}
	}	

	public void delete(Long id) {
		try {
			deleteStatement.setLong(1, id);
			
			deleteStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeAll(deleteStatement,null);
		}

	}



}

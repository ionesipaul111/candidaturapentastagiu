package AgentieFilmeInterogation;

import java.sql.*;

public class ConnectionFactory {

	private static String url = "jdbc:mysql://localhost:3306/";
	private static String dbName = "agentiefilme";
	private static String user = "root";
	private static String password = "";
	private static Connection con;

	private ConnectionFactory() {

	}

	public static Connection getConnection() throws InstantiationException,
			IllegalAccessException, ClassNotFoundException, SQLException {
		if (con == null) {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
			con = DriverManager.getConnection(url + dbName, user, password);
		}
		return con;
	}

	public static void closeAll(Statement createStatement, ResultSet rezult)
	{
		try{
			if(createStatement!=null)
				createStatement.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		try{
			if(rezult!=null)
				rezult.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	


}
package AgentieFilmeInterogation;

import java.sql.*;
import java.util.*;

import AgentieFilmeModel.Filme;
import AgentieFilmeModel.Genuri;

public class GenuriDAO {
	
	private final Connection connection;
	private final PreparedStatement insertStatement;
	private final PreparedStatement updateStatement;
	private final PreparedStatement deleteStatement;
	private final PreparedStatement loadStatement;
	private final PreparedStatement returnCodStat;
	
	public GenuriDAO(Connection connection) throws SQLException {
		this.connection = connection;
		loadStatement = connection
				.prepareStatement("SELECT * FROM `agentiefilme`.`genuri` WHERE id_gen = ?");
		insertStatement = connection.
				prepareStatement("INSERT INTO `agentiefilme`.`genuri` (`id_gen`, `nume_gen`) VALUES ( ?, ?);");
		updateStatement = connection
				.prepareStatement("UPDATE `agentiefilme`.`genuri` SET `nume_gen` = ? WHERE `id_gen` = ? ");
		deleteStatement = connection
				.prepareStatement("DELETE FROM `agentiefilme`.`genuri` WHERE id_gen = ?");
		returnCodStat = connection.prepareStatement("SELECT * FROM `agentiefilme`.`genuri` WHERE `nume_gen`=?");
	
	
	}
	
	public Genuri returnCod(String nume_gen){
		ResultSet result=null;
		try {
			returnCodStat.setString(1, nume_gen);
			result=returnCodStat.executeQuery();
			if(result.next()){
				Genuri gen = new Genuri();
				gen.setId_gen(result.getLong(1));
				gen.setNume_gen(result.getString(2));
				return gen;
			}else{
				return null;
			}
			
		} catch (SQLException e) {
			throw new RuntimeException("Error while retrieving gen with nume_gen: "
					+ nume_gen, e);
		} finally{
			ConnectionFactory.closeAll(returnCodStat, result);
		}
	}
	
	public Genuri load(Long id_gen) {
		ResultSet result=null;
	try {
		loadStatement.setLong(1, id_gen);
		result = loadStatement.executeQuery();
		if(result.next())
		{
			Genuri gen=new Genuri();
			gen.setNume_gen(result.getString(2));		
			return gen;
		}
		else {
			return null;
		}			
	} catch (SQLException e) {
		throw new RuntimeException("Error while retrieving gen with ID: "
				+ id_gen, e);
	}finally{
		ConnectionFactory.closeAll(loadStatement,result);
	}
}
	
	public List<Genuri> loadAll() {
		List<Genuri> genResult = new ArrayList<Genuri>();
		Statement createStatement=null;
		ResultSet result=null;

		try {
			createStatement = connection.createStatement();
			result = createStatement
					.executeQuery("SELECT * FROM `agentiefilme`.`genuri`");

			while(result.next()){
				Genuri gen = new Genuri();
				gen.setId_gen(result.getLong(1));
				gen.setNume_gen(result.getString(2));

				genResult.add(gen);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeAll(createStatement,result);
		}

		return genResult;
	}
	
	public void insert(Genuri gen){
		try{
			insertStatement.setLong(1,gen.getId_gen());
			insertStatement.setString(2, gen.getNume_gen());
			
			insertStatement.execute();
			
		}catch (SQLException e){
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeAll(insertStatement,null);
		}
		
	}
	
	public void update(Genuri gen){
		try{
		updateStatement.setLong(2, gen.getId_gen());
		updateStatement.setString(1, gen.getNume_gen());
		updateStatement.execute();
	
	}catch (SQLException e) {
		e.printStackTrace();
		}finally{
			ConnectionFactory.closeAll(updateStatement,null);
		}
	}	

	public void delete(Long id) {
		try {
			deleteStatement.setLong(1, id);
			
			deleteStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeAll(deleteStatement,null);
		}

	}



}

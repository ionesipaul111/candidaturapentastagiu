package AgentieFilmeInterogation;

import java.sql.*;
import java.util.*;


import AgentieFilmeModel.Actori;

public class ActoriDAO {
	
	private final Connection connection;
	private final PreparedStatement insertStatement;
	private final PreparedStatement updateStatement;
	private final PreparedStatement deleteStatement;
	private final PreparedStatement loadStatement;
	private final PreparedStatement searchStatement;
	private final PreparedStatement search1Statement;
	
	public ActoriDAO(Connection connection) throws SQLException {
		this.connection = connection;
		loadStatement = connection
				.prepareStatement("SELECT * FROM `agentiefilme`.`actori` WHERE id_actor = ?");
		insertStatement = connection.
				prepareStatement("INSERT INTO `agentiefilme`.`actori`(`nume_actor`,`data_nasteri`,`nationalitate`) VALUES ( ?, ?, ?);");
		updateStatement = connection
				.prepareStatement("UPDATE `agentiefilme`.`actori` SET `nume_actor` = ?, `data_nasteri` = ?, `nationalitate` = ? WHERE `id_actor` = ? ");
		deleteStatement = connection
				.prepareStatement("DELETE FROM `agentiefilme`.`actori` WHERE id_actor = ?");
		searchStatement =connection
				.prepareStatement("SELECT `a`.*,`c`.* FROM `agentiefilme`.`actori` `a` INNER JOIN `agentiefilme`.`distributie` `b` ON `a`.`id_actor` = `b`.`id_actor`"
                  +"INNER JOIN `agentiefilme`.`genuri` `c` ON `c`.`id_gen` = `b.id_gen`" +
                  "WHERE `a`.`id_actor`=?");
		search1Statement = connection
				.prepareStatement("SELECT * FROM `agentiefilme`.`actori`` WHERE `id_actor`=?" );
		
	}
	
	
	public List<Actori> search(Long id_actor){
		List<Actori> actoriResult = new ArrayList<Actori>();

		try {
			search1Statement.setLong(1,id_actor);
			ResultSet result= search1Statement.executeQuery();
			if(result.next())
				

				while(result.next()){
					Actori actor = new Actori();
					actor.setId_actor(result.getLong(1));
					actor.setNume_actor(result.getString(2));
					actor.setData_nasteri(result.getDate(3));
					actor.setNationalitate(result.getString(4));
					
					actoriResult.add(actor);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

			return actoriResult;
		}
	public Actori load(Long id_actor) {
			ResultSet result=null;
		try {
			loadStatement.setLong(1, id_actor);
			result = loadStatement.executeQuery();
			if(result.next())
			{
				Actori actor=new Actori();
				actor.setNume_actor(result.getString(2));
				actor.setData_nasteri(result.getDate(3));
				actor.setNationalitate(result.getString(4));
				return actor;
			}
			else {
				return null;
			}			
		} catch (SQLException e) {
			throw new RuntimeException("Error while retrieving Actor with ID: "
					+ id_actor, e);
		}finally{
			ConnectionFactory.closeAll(loadStatement,result);
		}
	}
	
	public List<Actori> loadAll() {
		List<Actori> actoriResult = new ArrayList<Actori>();
		Statement createStatement=null;
		ResultSet result=null;
		try {		
			createStatement = connection.createStatement();
			result = createStatement
					.executeQuery("SELECT * FROM `agentiefilme`.`actori`");

			while(result.next()){
				Actori actor = new Actori();
				actor.setId_actor(result.getLong(1));
				actor.setNume_actor(result.getString(2));
				actor.setData_nasteri(result.getDate(3));
				actor.setNationalitate(result.getString(4));
				
				actoriResult.add(actor);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeAll(createStatement,result);
		}

		return actoriResult;
	}
	
	public void insert(Actori actor){
		try{
			insertStatement.setString(1, actor.getNume_actor());
			insertStatement.setDate(2, new java.sql.Date(actor.getData_nasteri()
					.getTime()));
			insertStatement.setString(3, actor.getNationalitate());			
			insertStatement.execute();
			
		}catch (SQLException e){
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeAll(insertStatement,null);
		}
		
	}
	
	public void update(Actori actor){
		try{
		updateStatement.setLong(4, actor.getId_actor());
		updateStatement.setString(1, actor.getNume_actor());
		updateStatement.setDate(2, new java.sql.Date(actor.getData_nasteri()
				.getTime()));
		updateStatement.setString(3, actor.getNationalitate());
		
		updateStatement.execute();
	
	}catch (SQLException e) {
		e.printStackTrace();
		}
		finally{
			ConnectionFactory.closeAll(updateStatement,null);
		}
		
	}	

	public void delete(Long id) {
		try {
			deleteStatement.setLong(1, id);
			
			deleteStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		finally{
			ConnectionFactory.closeAll(deleteStatement,null);
		}

	}



}

package AgentieFilmeInterogation;

import java.sql.*;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UserDAO {

	private final Connection connection;
	private final PreparedStatement checkStatement;
	
	
	public UserDAO(Connection connection) throws SQLException{
		this.connection = connection;
		
		checkStatement = connection.prepareStatement("SELECT * FROM `agentiefilme`.`users` WHERE user=? AND password=?;");
	}
	
	public boolean validate(String username, String psw) {
		ResultSet result=null;
		boolean status = false;
		
		try {
			checkStatement.setString(1, username);
			checkStatement.setString(2, psw);
			
			result=checkStatement.executeQuery();
			status=result.next();
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			ConnectionFactory.closeAll(checkStatement, result);
		}
		
		return status;
	}
}

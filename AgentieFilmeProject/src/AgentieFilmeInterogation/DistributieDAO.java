package AgentieFilmeInterogation;

import java.sql.*;
import java.util.*;

import AgentieFilmeModel.Distributie;
import AgentieFilmeModel.Filme;

public class DistributieDAO {
	
	private final Connection connection;
	private final PreparedStatement insertStatement;
	private final PreparedStatement updateStatement;
	private final PreparedStatement deleteStatement;
	private final PreparedStatement loadStatement;
	private final PreparedStatement searchStatement;
	
	public  DistributieDAO(Connection connection) throws SQLException {
		this.connection = connection;
		loadStatement = connection
				.prepareStatement("SELECT * FROM `agentiefilme`.`distributie` WHERE id_film = ?");
		insertStatement = connection.
				prepareStatement("INSERT INTO `agentiefilme`.`distributie`(`id_film`,`id_gen`,`id_actor`,`rol`) VALUES ( ?, ?, ?, ?);");
		updateStatement = connection
				.prepareStatement("UPDATE `agentiefilme`.`distributie` SET `id_gen` = ?,`id_actor` = ?,`rol` = ? WHERE `id_film` = ?");
		deleteStatement = connection
				.prepareStatement("DELETE FROM `agentiefilme`.`distributie` WHERE id_film = ?");
		searchStatement = connection
				.prepareStatement("SELECT `g`.`nume_gen`,`d`.* FROM `agentiefilme`.`genuri` `g` " +
						"JOIN `distributie` `d` ON `g`.`id_gen`=`d`.`id_gen` WHERE `d`.`id_actor`= ? ");
		
	}
	
	public List<Distributie> search(long id_actor)	{
		ResultSet result=null;
		List<Distributie> list=new ArrayList<Distributie>();
		
		try {
			searchStatement.setLong(1,id_actor);
			result = searchStatement.executeQuery();
			
			while(result.next()){
				Distributie dist=new Distributie();
				dist.setNume_gen(result.getString(1));
				dist.setId_film(result.getLong(2));
				dist.setId_gen(result.getLong(3));
				dist.setId_actor(result.getLong(4));
				dist.setRol(result.getString(5));
				list.add(dist);
			}		
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeAll(searchStatement,result);
		}
		return list;
		
		
	}
	
	
	public Distributie load(Long id_film) {
		ResultSet result=null;
	try {
		loadStatement.setLong(1, id_film);
		result = loadStatement.executeQuery();
		if(result.next())
		{
			Distributie distributie=new Distributie();
			distributie.setId_gen(result.getLong(2));
			distributie.setId_gen(result.getLong(3));
			distributie.setRol(result.getString(4));
			
			return distributie;
		}
		else {
			return null;
		}			
	} catch (SQLException e) {
		throw new RuntimeException("Error while retrieving film with ID: "
				+ id_film, e);
	}finally{
		ConnectionFactory.closeAll(loadStatement,result);
	}
}
	
	public List< Distributie> loadAll() {
		List< Distributie> distributieResult = new ArrayList<Distributie>();
		Statement createStatement=null;
		ResultSet result=null;

		try {
			createStatement = connection.createStatement();
			result = createStatement
					.executeQuery("SELECT * FROM `agentiefilme`.`distributie`");

			while(result.next()){
				Distributie distributie = new  Distributie();
				distributie.setId_film(result.getLong(1));
				distributie.setId_gen(result.getLong(2));
				distributie.setId_actor(result.getLong(3));
				distributie.setRol(result.getString(4));

				
				distributieResult.add(distributie);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally{
			ConnectionFactory.closeAll(createStatement,result);
		}

		return  distributieResult;
	}
	
	public void insert(Distributie distributie){
		try{
			insertStatement.setLong(1,distributie.getId_film());
			insertStatement.setLong(2,distributie.getId_gen());
			insertStatement.setLong(3,distributie.getId_actor());
			insertStatement.setString(4,distributie.getRol());			
			
			insertStatement.execute();
			
		}catch (SQLException e){
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeAll(insertStatement,null);
		}
		
		
	}
	
	public void update(Distributie distributie){
		try{
		updateStatement.setLong(4, distributie.getId_film());
		updateStatement.setLong(1, distributie.getId_gen());
		updateStatement.setLong(2, distributie.getId_actor());
		updateStatement.setString(3, distributie.getRol());
		
		
		updateStatement.execute();
	
	}catch (SQLException e) {
		e.printStackTrace();
		}finally{
			ConnectionFactory.closeAll(updateStatement,null);
		}
	}	

	public void delete(Long id) {
		try {
			deleteStatement.setLong(1, id);
			
			deleteStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			ConnectionFactory.closeAll(deleteStatement,null);
		}

	}



}

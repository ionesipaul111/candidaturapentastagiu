-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 25, 2015 at 09:52 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `agentiefilme`
--

-- --------------------------------------------------------

--
-- Table structure for table `actori`
--

CREATE TABLE IF NOT EXISTS `actori` (
`id_actor` int(11) NOT NULL,
  `nume_actor` varchar(32) NOT NULL,
  `data_nasteri` date DEFAULT NULL,
  `nationalitate` varchar(32) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2014 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `actori`
--

INSERT INTO `actori` (`id_actor`, `nume_actor`, `data_nasteri`, `nationalitate`) VALUES
(1000, 'Vin Diesel', '1967-06-18', 'american'),
(1001, 'Paul Walker', '1973-09-12', 'american'),
(1002, 'Jason Statham', '1967-07-26', 'english'),
(1004, 'Dwayne Johnson', '1972-05-02', 'american'),
(2005, 'Sylvester Stallone', '1946-06-06', 'american'),
(2006, 'Harrison Ford', '1942-06-13', 'american'),
(2007, 'Arnold Schwarzenegger', '1947-06-30', 'american'),
(2008, 'Mel Gibson', '1956-01-03', 'american'),
(2009, 'Jesse Eisenberg', '1983-10-05', 'american'),
(2010, 'Mark Ruffalo', '1967-11-22', 'american'),
(2011, 'Woody Harrelson', '1961-07-23', 'american'),
(2012, 'Isla Fisher', '2015-05-25', 'omann'),
(2013, 'Morgan Freeman', '1967-06-01', 'american');

-- --------------------------------------------------------

--
-- Table structure for table `distributie`
--

CREATE TABLE IF NOT EXISTS `distributie` (
  `id_film` int(11) NOT NULL,
  `id_gen` int(11) NOT NULL,
  `id_actor` int(11) NOT NULL,
  `rol` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `distributie`
--

INSERT INTO `distributie` (`id_film`, `id_gen`, `id_actor`, `rol`) VALUES
(900, 111, 1000, 'principal'),
(900, 111, 1001, 'principal'),
(900, 111, 2003, 'principal'),
(901, 222, 1001, 'secundar'),
(906, 999, 1000, 'principal'),
(906, 999, 1001, 'principal'),
(907, 999, 1000, 'principal'),
(907, 999, 1001, 'principal'),
(908, 333, 1000, 'principal');

-- --------------------------------------------------------

--
-- Table structure for table `filme`
--

CREATE TABLE IF NOT EXISTS `filme` (
`id_film` int(11) NOT NULL,
  `id_gen` int(11) NOT NULL,
  `titlu_film_eng` varchar(32) NOT NULL,
  `titlu_film_ro` varchar(32) NOT NULL,
  `an_lansare` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=909 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `filme`
--

INSERT INTO `filme` (`id_film`, `id_gen`, `titlu_film_eng`, `titlu_film_ro`, `an_lansare`) VALUES
(900, 111, 'Furious Seven', 'Furios si iute 7', '2015-04-03'),
(901, 111, 'The Expendables 3', 'Eroi de sacrificiu 3', '2014-08-15'),
(902, 111, 'Now You See Me', 'Jaful perfect', '2013-05-31'),
(903, 333, 'Wedding Ringer', 'Cavaler de inchiriat', '2014-12-10'),
(906, 999, 'Fast & Fourios 6', 'Furios si iute 6', '2013-05-18'),
(907, 999, 'Fast Five', 'Furios si iute 5', '2011-06-25'),
(908, 333, 'Find my guilty', 'Nevinpvatul', '2006-01-05');

-- --------------------------------------------------------

--
-- Table structure for table `genuri`
--

CREATE TABLE IF NOT EXISTS `genuri` (
  `id_gen` int(11) NOT NULL,
  `nume_gen` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `genuri`
--

INSERT INTO `genuri` (`id_gen`, `nume_gen`) VALUES
(111, 'actiune'),
(222, 'Horror'),
(333, 'comedie'),
(444, 'aventura'),
(555, 'drama'),
(666, 'istorice'),
(777, 'mister'),
(888, 'razboi'),
(999, 'thriller');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user`, `password`) VALUES
('Paul', '1234');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actori`
--
ALTER TABLE `actori`
 ADD PRIMARY KEY (`id_actor`);

--
-- Indexes for table `filme`
--
ALTER TABLE `filme`
 ADD PRIMARY KEY (`id_film`);

--
-- Indexes for table `genuri`
--
ALTER TABLE `genuri`
 ADD PRIMARY KEY (`id_gen`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actori`
--
ALTER TABLE `actori`
MODIFY `id_actor` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2014;
--
-- AUTO_INCREMENT for table `filme`
--
ALTER TABLE `filme`
MODIFY `id_film` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=909;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
